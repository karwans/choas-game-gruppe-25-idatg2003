package edu.ntnu.stud.idatg2003;

/**
 * Entry point for running the Chaos Game application.
 * This class serves as a simple runner to launch the ChaosGameGUI.
 *
 * @version 0.0.3
 * @since 0.0.3 (The version of Chaos-Game application when introduced)
 */
public class ChaosGameRunner {

  /**
   * The main method to launch the Chaos Game GUI application.
   *
   * @param args the command line arguments
   * @since 0.0.1
   */
  public static void main(String[] args) {
    ChaosGameGui.main(args); // Delegate to the main method of ChaosGameGUI
  }
}
